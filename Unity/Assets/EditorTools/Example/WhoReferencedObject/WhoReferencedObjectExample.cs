﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhoReferencedObjectExample : MonoBehaviour
{
    [Header("tott")]
    [SerializeField]
    Transform target;
    [Space(100)]
    public A test;
    [NonSerialized]
    public A[] tests;

    [Space(100)]
    public Transform xxx;
}
[System.Serializable]
public class A
{
    public B b;
}
[System.Serializable]
public class B
{
    [Header("9999")]
    public C c;
}
[System.Serializable]
public class C
{
    public Transform transform;
}
