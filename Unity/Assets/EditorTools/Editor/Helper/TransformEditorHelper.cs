﻿
using UnityEditor;
using UnityEngine;

public static class TransformEditorHelper
{
    public static Transform [] SceneTransforms()
    {
        var allGos = Resources.FindObjectsOfTypeAll<GameObject>();
        var oldSelect = Selection.objects;
        Selection.objects = allGos;
        var transforms = Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab);
        Selection.objects = oldSelect;

        return transforms;
    }
}