﻿
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class FindReferencedHelper
{
    public static Object[] FindReferenced(this Object obj)
    {
        return  Find(new SerializedObject(obj));
    }
  
    public static Object[] Find(SerializedObject obj)
    {
        var prop = obj.GetIterator();
        List<Object> result = new List<Object>();
        while (prop.Next(true))
        {
            if (prop.propertyType == SerializedPropertyType.ObjectReference)
            {
                var value = prop.objectReferenceValue;
                if (value == null)
                    continue;
                result.Add(value);
            }
        }
        return result.ToArray();
    }
}
