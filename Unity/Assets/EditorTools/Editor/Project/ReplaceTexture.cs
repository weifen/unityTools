﻿using System.IO;
using System.Linq;
using UnityEditor;

public class ReplaceTexture:Editor
{
    static string directory;
    static string[] suffix = new string[] {".jpg",".png",".jpeg",".bmp" };
    [MenuItem("Assets/替换图片")]
    static void Run()
    {
        if (Selection.activeObject == null)
            return;
        var activePath = AssetDatabase.GetAssetPath(Selection.activeObject);
        if(!suffix.Contains(Path.GetExtension(activePath)))
        {
            return;
        }
        
        var path = EditorUtility.OpenFilePanel("选择替换图片文件", directory, "jpg,png,jpeg,bmp");
        if (File.Exists(path))
        {
            directory = Path.GetDirectoryName(path);
            File.Copy(path, activePath, true);
            AssetDatabase.Refresh();

        }
    }
}