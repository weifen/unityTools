﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TextureToSprite : Editor
{
    static string[] array = new string[] { ".png", ".jpg" };
    [MenuItem("Assets/图片转换为精灵")]
    static void Run()
    {

        List<string> list = new List<string>();
        foreach (var obj in Selection.objects)
        {
            var dirPath = AssetDatabase.GetAssetPath(obj);
            if (Directory.Exists(dirPath))
            {
                var paths = FileHelper.GetFiles( dirPath, array);
                foreach (var item in paths)
                {
                    list.Add(item);
                }
            }
            else
            {
                list.Add(dirPath);
            }
        }
        int index = 0;
        foreach (var item in list)
        {
            var ai = TextureImporter.GetAtPath(item) as TextureImporter;
            if (ai == null)
                continue;
            if (ai.textureType == TextureImporterType.Sprite)
                continue;
            ai.textureType = TextureImporterType.Sprite;
            AssetDatabase.ImportAsset(item);
            EditorUtility.DisplayCancelableProgressBar("图片转换为精灵", item, (float)index++ / list.Count);
        }
       
        EditorUtility.ClearProgressBar();
        AssetDatabase.Refresh();
    }
}
