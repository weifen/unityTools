﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

public class NotepadOpenFile : Editor {

	[MenuItem("Assets/文本方式打开文件")]
	static void ShowFile()
	{
		Process.Start("notepad.exe", AssetDatabase.GetAssetPath(Selection.activeObject));
	}
	[MenuItem("Assets/打开meta")]
	static void ShowMeta()
	{
		Process.Start("notepad.exe", AssetDatabase.GetAssetPath(Selection.activeObject)+".meta");
	}
}
