﻿using UnityEditor;

[ToolButton("图片处理","显示图片列表")]
public class ReferencesTextureButton : IToolsButtonEvent
{
    public void Run()
    {
        var window = EditorWindow.GetWindow<ShowTextureReferencesWindow>("图片列表");
        window.LoadReferences();
        window .Show();
    }
}

