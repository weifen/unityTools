﻿using System;
using UnityEditor;
using UnityEngine;

namespace MyGUI
{
    public class Table : IRenderer
    {
        static Color oneLineColor = new Color32(0, 255, 150, 50);
        static Color twoLineColor = new Color32(150, 0, 255, 50);
        static Color selectLineColor = new Color32(180, 150, 50, 100);

        int _index=-1;


        bool isIndexChanged;
        Vector2 pos;
        int lineCount;
        TreeNode<ITableLine> curNode;
        ListTabelTitle listTabelTitle;
        TreeNode<ITableLine> root = new TreeNode<ITableLine>(new LineData());
        public Action<int, TreeNode<ITableLine>> onIndexChanged;

        public Table()
        {
            listTabelTitle = new ListTabelTitle();
            listTabelTitle.Add("序号", 40);
            listTabelTitle.Add("折叠", 40);
            listTabelTitle.Add("选择", 40);
            listTabelTitle.Add("选中", 60);
            listTabelTitle.Add("对象", 40);

            listTabelTitle.SetRenderer(0,(rect,node)=> GUI.Label(rect, lineCount.ToString()));
            listTabelTitle.SetRenderer(1,(rect,node)=> node.Value.isOpen = EditorGUI.Foldout(rect, node.Value.isOpen, ""));
            listTabelTitle.SetRenderer(2,(rect,node)=> node.Value.isToggle = EditorGUI.ToggleLeft(rect, "", node.Value.isToggle));
            listTabelTitle.SetRenderer(3,(rect,node)=> GUI.Button(rect, "选择对象"));
            listTabelTitle.SetRenderer(4,(rect,node)=> GUI.Label(rect, node.Value.title));

        }

        public int index
        {
            get { return _index; }
            set
            {
                _index = value;
                isIndexChanged = true;
                if (onIndexChanged!=null)
                {
                    onIndexChanged.Invoke(value, curNode);
                }
            }
        }

        public void SetListTabelTitle(ListTabelTitle value)
        {
            listTabelTitle = value;
        }

        public void AddTreeNode(TreeNode<ITableLine> node)
        {
            root.Add(node);
        }

        public void SetRoot(TreeNode<ITableLine> node)
        {
            root = node;
        }

        public void RemoveTreeNode(TreeNode<ITableLine> node)
        {
            root.Remove(node);
        }

        public void RemoveAtTreeNode(int index)
        {
            root.RemoveAt(index);
        }

        public void Renderer()
        {
            isIndexChanged = false;
            var rect = EditorGUILayout.BeginVertical("Box");
            {
                DrawTableTitle();
                pos = EditorGUILayout.BeginScrollView(pos);
                {
                    lineCount = 0;
                    TreeNodeHelper.Ergodic(root, DrawNode);
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();

            for (int i = 0; i < listTabelTitle.Count - 1; i++)
            {
                var boxRect = listTabelTitle.GetRect(rect, i);
                EditorGUI.DrawRect(new Rect(boxRect.x + boxRect.width + 5, rect.y, 1, rect.height),Color.gray);
            }

            if(!isIndexChanged)
            {
                IndexEvent(rect);
            }
        }

        void DrawTableTitle()
        {
            var rect = EditorGUILayout.GetControlRect();

            var labelAlignment = GUI.skin.label.alignment;
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;


            for (int i = 0; i < listTabelTitle.Count; i++)
            {
                GUI.Label(listTabelTitle.GetRect(rect, i), listTabelTitle[i]);
            }

            GUI.skin.label.alignment = labelAlignment;
        }


        bool DrawNode(TreeNode<ITableLine> node)
        {
            var rect = EditorGUILayout.GetControlRect();
            if (index != lineCount)
            {
                EditorGUI.DrawRect(rect, lineCount % 2 == 0 ? oneLineColor : twoLineColor);
            }
            else
            {
                EditorGUI.DrawRect(rect, selectLineColor);
            }

            for (int i = 0; i < listTabelTitle.Count; i++)
            {
                listTabelTitle.Renderer(i, listTabelTitle.GetRect(rect, i), node);

            }

            GUIEvent(rect,node);

            lineCount++;
            return node.Value.isOpen;
        }

        void GUIEvent(Rect rect, TreeNode<ITableLine> node)
        {
            curNode = node;
            var e = Event.current;
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (rect.Contains(e.mousePosition) && e.button == 0)
                    {
                        index = lineCount;
                        e.Use();
                    }
                    break;

            }
            curNode = null;
        }

        void IndexEvent(Rect rect)
        {
            var e = Event.current;
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (rect.Contains(e.mousePosition) && e.button == 0)
                    {
                        index = -1;
                        e.Use();
                    }
                    break;
            }
        }


    }
}