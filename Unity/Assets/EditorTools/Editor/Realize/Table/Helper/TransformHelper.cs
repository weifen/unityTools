﻿using UnityEngine;

namespace MyGUI
{
    public sealed class TransformHelper
    {
        public static TreeNode<ITableLine> Foreach(Transform transform)
        {
            TreeNode<ITableLine> node = new TreeNode<ITableLine>(new LineGameObject(transform.gameObject));
            foreach(Transform child in transform)
            {
                node.Add( Foreach(child));
            }
            return node;
        }
    }
}

