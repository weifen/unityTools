﻿namespace MyGUI
{
    public interface ITableLine
    {
        bool isOpen { get; set; }
        bool isToggle { get; set; }
        string title { get; }
    }
}