﻿using UnityEngine;

namespace MyGUI
{
    public interface IRenderer
    {
        void Renderer();
    }
    public interface IRenderer<T>
    {
        void Renderer(Rect rect, T data);
    }
    

}