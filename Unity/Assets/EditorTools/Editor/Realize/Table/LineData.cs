﻿using UnityEngine;

namespace MyGUI
{
    public class LineData: ITableLine
    {
        public bool isOpen { get; set; }
        public bool isToggle { get; set; }
        public string title { get; private set; }
        public LineData()
        {
            isOpen = true;
            isToggle = false;
        }
        public LineData(string title)
        {
            isOpen = true;
            isToggle = false;
            this.title = title;
        }
    }


    public class LineGameObject : LineData, ITableLine
    {
        public GameObject gameObject;
        public LineGameObject(GameObject gameObject):base(gameObject.name)
        {
            this.gameObject = gameObject;
        }
    }
}