﻿using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace ChangeSkin
{
    [ToolButton("文本处理", "更改All代码编码 UTF-8")]
    public class ChangeAllCodeEncodingEditor:IToolsButtonEvent
    {
        public void Run()
        {
            var files = Directory.GetFiles(Application.dataPath, "*.cs", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                File.WriteAllText(file, File.ReadAllText(file), Encoding.UTF8);
            }
            AssetDatabase.Refresh();
            Debug.LogError("更改完成");
        }
    }
}