﻿using System.Collections;
using System.Collections.Generic;
//using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[ToolButton("文本处理", "更新组件配置")]
public class ImportTextComponent : IToolsButtonEvent
{
    const string s_path = "GameConfigure/TextComponent.xlsx";
    FontManger fontManger;
    public void Run()
    {
        var excel = ExcelHelper.LoadExcel(s_path);
        var tabel = excel.Tables[0];
        var managers = Resources.FindObjectsOfTypeAll<FontManger>();
        if (managers.Length > 0)
        {
            fontManger = managers[0];
        }
        else
        {
            Debug.LogError("Not Find FontManager");
            return;
        }

        for (int i = 2; i < tabel.NumberOfColumns; i++)
        {
            var column = 1;
            var filepath = tabel.GetValue(i, column++);
            var path = tabel.GetValue(i, column++);
            var fontName = tabel.GetValue(i, column++);
            var fontSize =int.Parse(  tabel.GetValue(i, column++));
            var fontColor = tabel.GetValue(i, column++).HexToColor();
            int drawSize;
            int.TryParse(tabel.GetValue(i, column++),out drawSize);
            var tmp = tabel.GetValue(i, column++);
            Color drawColor=Color.white;
            if (!string.IsNullOrEmpty(tmp))
            {
                drawColor = tmp.HexToColor();
            }

            var suffix = Path.GetExtension(filepath);
            
            if (suffix == ".prefab")
            {
                 var text = Prefab(filepath, path);
                UpdateText(text, fontName, fontSize, fontColor, drawSize, drawColor);
                AssetDatabase.SaveAssets();
            }
            else
            {
                var text= Scene(filepath, path);
                UpdateText(text, fontName, fontSize, fontColor, drawSize, drawColor);
                EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            }
            
        }
        
        AssetDatabase.Refresh();
        Debug.Log("更新完成");
    }

    Text Prefab(string filePath, string path)
    {
        var index = path.IndexOf("/");
        if (index != -1)
        {
            path = path.Substring(index + 1);
        }
        Text text;
        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(filePath);
        if (index == -1)
        {
            text = prefab.GetComponent<Text>();
        }
        else
        {
            var go = prefab.transform.Find(path);
            text = go.GetComponent<Text>();
        }
        return text;
    }

    Text Scene(string filePath, string path)
    {
        EditorSceneManager.OpenScene(filePath);
        var go = GameObject.Find(path);
        if (go == null)
            return null;
        
        return go.GetComponent<Text>();
    }

    void UpdateText(Text text, string fontName, int fontSize, Color fontColor, int drawSize, Color drawColor)
    {
        text.font = fontManger.EditorFont(fontName);
        text.fontSize = fontSize;
        text.color = fontColor;
        if(drawSize>0)
        {
            var online = text.GetComponent<Outline>();
            if(online==null)
            {
                online = text.gameObject.AddComponent<Outline>();
            }
            online.effectColor = drawColor;
            online.effectDistance = new Vector2(drawSize,-drawSize);
        }

        EditorUtility.SetDirty(text);
    }

}