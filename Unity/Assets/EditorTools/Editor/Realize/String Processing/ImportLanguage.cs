﻿//using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

[ToolButton("文本处理","导入翻译文本")]
public class ImportText : IToolsButtonEvent
{
    const string path = "GameConfigure/Language.xlsx";
    public void Run()
    {
        var excel = ExcelHelper.LoadExcel(path);
        var tabel = excel.Tables[0];

        var managerPath = Directory.GetFiles(Application.dataPath, "LanguageManager.prefab", SearchOption.AllDirectories)
        .Where(s => s.IndexOf(@"EditorTools\Res\LanguageManager.prefab") != -1).FirstOrDefault();


        List<LanguageInfo> list = new List<LanguageInfo>();
        for (int i = 4; i < tabel.NumberOfRows;i++)
        {
            list.Add(new LanguageInfo
            {
                key = tabel.GetValue(i, 1),
                chinese = tabel.GetValue(i, 2)
            }); 
        }
        var manager = AssetDatabase.LoadAssetAtPath<LanguageManager>(managerPath.Substring(managerPath.IndexOf("Assets")));
        manager.array = list.ToArray();
        EditorUtility.SetDirty(manager);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        Debug.Log("导入翻译文本完成");
    }

}

