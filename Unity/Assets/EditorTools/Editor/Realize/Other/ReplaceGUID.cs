﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

[ToolButton("其他","替换组件")]
public class ReplaceComponent : IToolsButtonEvent
{
    public void Run()
    {
        EditorWindow. GetWindow<ReplaceGUID>("替换组件").Show();
    }
}

public class ReplaceGUID : EditorWindow
{
    static string[] suffixList = new string[] { ".prefab", ".unity", ".mat", ".asset" };

    string oldfileid;
    string oldguid;
    string newfileid;
    string newguid;
    private void OnGUI()
    {

        var color = GUI.skin.label.normal.textColor;
        GUI.skin.label.normal.textColor = Color.red;
        GUILayout.Label("新的组件必须继承自被替换的组件才不会丢失引用");
        GUI.skin.label.normal.textColor = color;
        EditorGUILayout.BeginVertical("Box");
        {
            GUILayout.Label("原来的GUID");
            oldfileid = EditorGUILayout.TextField("fileid", oldfileid);
            oldguid = EditorGUILayout.TextField("guid", oldguid);
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical("Box");
        {
            GUILayout.Label("替换的GUID");
            EditorGUILayout.LabelField("非DLL里面的组件默认fileid", "11500000");
            newfileid = EditorGUILayout.TextField("fileid", newfileid);
            newguid = EditorGUILayout.TextField("guid", newguid);
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        if (GUILayout.Button("替换"))
        {
            if (string.IsNullOrEmpty(oldfileid) || string.IsNullOrEmpty(oldguid))
            {
                return;
            }
            if (string.IsNullOrEmpty(newfileid) || string.IsNullOrEmpty(newguid))
            {
                return;
            }
            Replace(oldfileid,oldguid,newfileid,newguid);
        }
    }

    void Replace(string oldfileid,string oldguid, string newfileid, string newguid)
    {
        var oldScript = string.Format("m_Script: {{fileID: {0}, guid: {1}, type: 3}}", oldfileid, oldguid);
        var newScript = string.Format("m_Script: {{fileID: {0}, guid: {1}, type: 3}}", newfileid, newguid);

        var files = FileHelper.GetFiles(suffixList);

        int startIndex = 0;
        EditorApplication.update = delegate ()
        {
            string file = files[startIndex];

            bool isCancel = EditorUtility.DisplayCancelableProgressBar("匹配资源中", file, (float)startIndex / (float)files.Length);

            if (Regex.IsMatch(File.ReadAllText(file), oldScript))
            {
                // Debug.Log(file, AssetDatabase.LoadAssetAtPath<Object>(GetRelativeAssetsPath(file)));
                //Debug.Log(file);
                 var result = File.ReadAllText(file).Replace(oldScript,newScript);
                File.WriteAllText(file, result);
            }

            startIndex++;
            if (isCancel || startIndex >= files.Length)
            {
                EditorUtility.ClearProgressBar();
                EditorApplication.update = null;
                startIndex = 0;
                Debug.Log("匹配结束");
                AssetDatabase.Refresh();
            }

        };

    }

     private string GetRelativeAssetsPath(string path)
    {
        return "Assets" + Path.GetFullPath(path).Replace(Path.GetFullPath(Application.dataPath), "").Replace('\\', '/');
    }

}
