﻿using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class ReplaceFont :Editor
{
    static string[] suffix = new string[] {".prefab",".unity" };
    [MenuItem("Assets/替换全部字体",priority =100)]
    static void Run()
    {
        var font = AssetDatabase.LoadAssetAtPath<Font>(AssetDatabase.GetAssetPath(Selection.activeObject));
        if(font==null)
        {
            Debug.LogError("选择的不是字体");
            return;
        }

        var array  = AssetHelper.GetPrefab<Text>();
        int index = 0;
        foreach ( var item in array)
        {
            item.font = font;
            item.fontStyle = FontStyle.Normal;
            var path = AssetDatabase.GetAssetPath(item);
            EditorUtility.DisplayProgressBar("替换字体", path, (float)index++ / array.Length);
            EditorUtility.SetDirty(item);
        }
        AssetDatabase.SaveAssets();

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        foreach( var scene in EditorBuildSettings.scenes)
        {
            var activeScene = EditorSceneManager.OpenScene(scene.path);
            array = AssetHelper.GetHierachy<Text>();
            foreach(var item in array)
            {
                item.font = font;
                item.fontStyle = FontStyle.Normal;
                EditorUtility.DisplayProgressBar("替换字体", item.transform.GetPath(), (float)index++ / array.Length);
            }
            EditorSceneManager.SaveScene(activeScene);
        }
        EditorUtility.ClearProgressBar();
        AssetDatabase.Refresh();
    }
}
