﻿using System.IO;
using UnityEngine;

[ToolButton("其他", "清空persistentData")]
public class ClearPersistentData : IToolsButtonEvent
{
    public void Run()
    {
        Directory.Delete(Application.persistentDataPath,true);
    }
}
