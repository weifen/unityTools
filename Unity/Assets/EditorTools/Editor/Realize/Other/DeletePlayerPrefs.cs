﻿using UnityEngine;

[ToolButton("其他", "清空PlayerPrefs数据")]
public class DeletePlayerPrefs : IToolsButtonEvent
{
    public void Run()
    {
        PlayerPrefs.DeleteAll();
    }
}
