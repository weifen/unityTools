﻿
using UnityEditor;
using UnityEngine;

public class ReferencedWindow:EditorWindow
{
    Vector2 pos;
    public GameObject gameObject;
    public ObjectReferenced[] array;

    private void OnGUI()
    {
        if (gameObject == null || array == null)
            return;
        pos  = EditorGUILayout.BeginScrollView(pos);
        {
            GUI.enabled = false;
            EditorGUILayout.ObjectField("查询对象", gameObject, typeof(GameObject), false);
            GUI.enabled = true;
            foreach(var item in array)
            {
                if (item.objects.Length <= 0)
                    continue;
                EditorGUILayout.BeginVertical("Box");
                {
                    GUI.enabled = false;
                    EditorGUILayout.ObjectField("组件", item.@object, typeof(Object), false);
                    GUI.enabled = true;
                    EditorGUILayout.Space();
                    foreach (var obj in item.objects)
                    {
                        if(GUILayout.Button(obj.name))
                        {
                            Selection.activeObject = obj;
                        }
                    }
                }
                EditorGUILayout.EndVertical();
            }
        }
        EditorGUILayout.EndScrollView();
    }
}
