﻿using System;

public class ToolButtonAttribute : Attribute
{
    public string type { get; private set; }
    public string title { get; private set; }

    public ToolButtonAttribute(string type, string title)
    {
        this.type = type;
        this.title = title;
    }
}


//public class ReplaceScript : Editor
//{
//    [MenuItem("CONTEXT/MonoBehaviour/替换脚本")]
//    static void Run(MenuCommand command)
//    {
       
//    }
//}   