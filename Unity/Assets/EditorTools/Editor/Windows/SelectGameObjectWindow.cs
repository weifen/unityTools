﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SelectGameObjectWindow : EditorWindow
{
    public string value;

    private void OnGUI()
    {
        value = EditorGUILayout.TextField("名字", value);
        if (GUILayout.Button("选择"))
        {
            List<GameObject> list = new List<GameObject>();
            foreach (GameObject obj in Selection.objects)
            {
                var tr = obj.transform.Find(value);
                if (tr == null)
                    continue;
                list.Add(tr.gameObject);
            }
            Selection.objects = list.ToArray();
        }
    }
}
