﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

//[CustomEditor(typeof(Animator))]
public class CustomAnimatorEditor : Editor
{
    Vector2 pos;
    GameObject go;
    AnimationClip clip;
    float value;
    float maxValue;
    State state;
    AnimatorController animatorController;
    private void OnEnable()
    {
        if (EditorApplication.isPlaying)
            return;
        var animator = target as Animator;
        animatorController = animator.runtimeAnimatorController as AnimatorController;
        go = animator.gameObject;
        AnimationMode.StartAnimationMode();
        EditorApplication.update += Update;
    }

    private void OnDisable()
    {
        if (EditorApplication.isPlaying)
            return;
        EditorApplication.update -= Update;
        AnimationMode.StopAnimationMode();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (EditorApplication.isPlaying)
            return;
        if (animatorController == null)
            return;
        EditorGUILayout.Space();
        pos = EditorGUILayout.BeginScrollView(pos);
        {
            foreach (var item in animatorController.animationClips)
            {
                if (GUILayout.Button(item.name))
                {
                    SelectClip(item);
                }
            }
        }
        EditorGUILayout.EndScrollView();
        
        GUILayout.Space(20);
        if (state == State.Playing)
        {
            EditorGUILayout.Slider(value, 0, maxValue);
        }
        else
        {
            value = EditorGUILayout.Slider(value, 0, maxValue);
        }

        EditorGUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("开始"))
            {
                state = State.Playing;
            }
            if (GUILayout.Button("暂停"))
            {
                state = State.Pause;
            }
        
            if (GUILayout.Button("结束"))
            {
                state = State.Stop;
                value = 0;
                UpdateSampling();
                
            }
        }
        EditorGUILayout.EndHorizontal();
    }

    void SelectClip(AnimationClip clip)
    {
        this.clip = clip;
        maxValue = clip.length;
        state = State.Playing;
        value = 0;
    }

    double oldtime;
    void Update()
    {
        if (go == null || clip == null || EditorApplication.isPlaying)
            return;

        switch (state)
        {
            case State.Playing:
                value += (float)(EditorApplication.timeSinceStartup - oldtime);
                oldtime = EditorApplication.timeSinceStartup;
                if (value >= clip.length)
                {
                    value = 0;
                }
                this.Repaint();
                break;
            case State.Stop:
            case State.None:
            case State.Pause:
                oldtime = EditorApplication.timeSinceStartup;
                break;
        }
        UpdateSampling();
    }

    void UpdateSampling()
    {
        AnimationMode.BeginSampling();
        AnimationMode.SampleAnimationClip(go, clip, value);
        AnimationMode.EndSampling();
    }

    enum State
    {
        None,
        Playing,
        Stop,
        Pause
    }
}
