﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(MonoBehaviour))]
public class ReferenceColor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUI.DrawRect(new Rect(0,0,100,100), Color.red);
        base.OnInspectorGUI();
    }
}
