﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BeginSearch : Editor
{
    public static Transform beginTransform;
    [MenuItem("GameObject/从该对象开始搜索",priority = 11)]
    public static void Run()
    {
        beginTransform = Selection.activeTransform;
    }
}
