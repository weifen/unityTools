﻿using UnityEditor;

namespace ChangeSkin
{
    [ToolButton("Hierarchy层级", "顶层")]
    public class GameObjectLastSibling : IToolsButtonEvent
    {
        public void Run()
        {
            Selection.activeTransform.SetAsLastSibling();
        }
    }
}