﻿
using UnityEditor;
using UnityEngine;

namespace ChangeSkin
{
    [ToolButton("Hierarchy", "保存Prefab")]
    public class SavePrefab : IToolsButtonEvent
    {
        public void Run()
        {
            foreach (var item in Selection.gameObjects)
            {
#if UNITY_5 ||UNITY_2017
                var obj = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GetAssetPath(item));
#else
                var obj = PrefabUtility.GetCorrespondingObjectFromSource(item);
                PrefabUtility.SaveAsPrefabAsset(item, AssetDatabase.GetAssetPath(obj));
                obj = PrefabUtility.GetCorrespondingObjectFromSource(item);
#endif
                obj.transform.localPosition = item.transform.localPosition;
                obj.transform.localRotation = item.transform.localRotation;
                obj.transform.localScale = item.transform.localScale;
                EditorUtility.SetDirty(obj);
                AssetDatabase.SaveAssets();

                Debug.Log("保存" + item.name);
            }

        }
    }

}