﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectReferenced
{
    public Object @object { get;private set; }
    public Object[] objects { get; private set; }

    public ObjectReferenced(Object @object,params Object[] arg)
    {
        this.@object = @object;
        List<Object> result = new List<Object>();
        foreach(var item in  @object.FindReferenced())
        {
            if (arg.Contains(item))
                continue;
            result.Add(item);
        }
        objects = result.ToArray();
    }
}