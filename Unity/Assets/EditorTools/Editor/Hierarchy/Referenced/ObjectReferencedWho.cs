﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ObjectReferencedWho : Editor
{
    [MenuItem("GameObject/对象引用了谁", priority = 13)]
    static void Run()
    {
        if (Selection.activeGameObject == null)
            return;

        var gameObject = Selection.activeGameObject;
        List<ObjectReferenced> result = new List<ObjectReferenced>();

        List<Object> list = new List<Object>();
        list.Add(Selection.activeGameObject);
        list.AddRange(Selection.activeGameObject.GetComponents<Component>());

        foreach (var item in list)
        {
            try
            {
                result.Add(new ObjectReferenced(item, list.ToArray()));

            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        var window = EditorWindow.GetWindow<ReferencedWindow>("对象引用了谁");
        window.gameObject = gameObject;
        window.array = result.ToArray();
        window.Show();
    }
}
