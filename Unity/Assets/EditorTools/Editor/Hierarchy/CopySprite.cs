﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
//using UnityEditor.Experimental;
using UnityEngine;
using UnityEngine.UI;

public class CopySprite : Editor
{
    [MenuItem("GameObject/复制Sprite &c",priority =15)]
    static void Run()
    {
        if (Selection.activeGameObject == null)
            return;
         var img= Selection.activeGameObject.GetComponent<Image>();
        if (img == null || img.sprite == null)
            return;

        EditorGUIUtility.systemCopyBuffer = AssetDatabase.GetAssetPath(img.sprite);
       
    }
}


public class PasteSprite:Editor
{
    [MenuItem("GameObject/粘贴Sprite &v", priority = 16)]
    static void Run()
    {
        if (Selection.activeGameObject == null)
            return;
        var img = Selection.activeGameObject.GetComponent<Image>();
        if (img == null )
            return;

        var path = EditorGUIUtility.systemCopyBuffer;
        if (string.IsNullOrEmpty(path))
            return;
        Debug.LogError(EditorGUIUtility.systemCopyBuffer);
        var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(EditorGUIUtility.systemCopyBuffer);
        Debug.LogError(sprite);
        img.sprite = sprite;
    }
}