﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ChangeSkin
{
    [ToolButton("Hierarchy层级", "上移一层")]
    public class GameObjectUp : IToolsButtonEvent
    {
        public void Run()
        {
            Selection.activeTransform.SetSiblingIndex(Selection.activeTransform.GetSiblingIndex() + 1);
        }
    }
}