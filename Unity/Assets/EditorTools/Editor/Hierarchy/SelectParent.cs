﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
//using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace ChangeSkin
{
    [ToolButton("Hierarchy对象", "选择父对象")]
    public class SelectParent : IToolsButtonEvent
    {
        public void Run()
        {
            if (Selection.activeGameObject == null)
                return;

            //Selection.activeTransform = Selection.activeTransform.parent;
            List<Transform> result = new List<Transform>();
            foreach(var item in Selection.gameObjects)
            {
                result.Add(item.transform.parent);
            }
            Selection.objects = result.ToArray();
        }
    }
}

