﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ToolButton("Hierarchy", "选择对象")]
public class SelectGameObject : IToolsButtonEvent
{
    public void Run()
    {
        SelectGameObjectWindow.GetWindow<SelectGameObjectWindow>("选择对象").Show();
    }

   
}

