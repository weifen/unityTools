﻿using UnityEditor;

namespace ChangeSkin
{
    [ToolButton("Hierarchy层级", "底层")]
    public class GameObjectFirstSibling : IToolsButtonEvent
    {
        public void Run()
        {
            Selection.activeTransform.SetAsFirstSibling();
        }
    }
}