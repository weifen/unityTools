﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EndSearch : Editor
{

    [MenuItem("GameObject/结束搜索", priority = 11)]
    public static void Run()
    {
        if (BeginSearch.beginTransform == null)
        {
            Debug.LogError("没有选择开始对象");
            return;
        }
        if (Selection.activeObject == null)
        {
            Debug.LogError("搜索了个寂寞");
            return;
        }
        var path = Selection.activeTransform.GetPath(BeginSearch.beginTransform);
        var index = path.IndexOf("/");
        if (index != -1)
        {
            path = path.Substring(index + 1);
        }
        var window = EditorWindow.GetWindow<SelectGameObjectWindow>("选择对象");
        window.value = path;
        window.Show();
    }
}
