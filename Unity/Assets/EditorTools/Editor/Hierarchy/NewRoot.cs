﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace ChangeSkin
{
    [ToolButton("Hierarchy对象", "选中多个同一目录对象新建一个父对象")]
    public  class NewRoot:IToolsButtonEvent
    {
        public void Run()
        {
            if (Selection.activeGameObject==null)
                return;

            var root = new GameObject("Root").transform;
            var parent = Selection.activeTransform.parent;
            root.SetParent(parent, false);
            foreach(var item in Selection.gameObjects)
            {
                item.transform.SetParent(root, false);
            }
        }
    }
}
