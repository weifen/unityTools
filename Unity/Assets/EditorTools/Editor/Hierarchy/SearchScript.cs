﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ToolButton("Hierarchy", "搜索脚本")]
public class SearchScript : IToolsButtonEvent
{
    public void Run()
    {
        EditorWindow.GetWindow<SearchScriptWindow>("搜索脚本").Show();
    }
}
