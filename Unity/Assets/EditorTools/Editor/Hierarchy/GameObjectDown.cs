﻿using UnityEditor;

namespace ChangeSkin
{
    [ToolButton("Hierarchy层级", "下移一层")]
    public class GameObjectDown : IToolsButtonEvent
    {
        public void Run()
        {
            var index = Selection.activeTransform.GetSiblingIndex();
            if (index == 0)
                return;

            Selection.activeTransform.SetSiblingIndex(index - 1);
        }
    }
}