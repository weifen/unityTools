﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FontManger : MonoSingleton<FontManger>
{
    public List<Font> array;
    Dictionary<string, Font> pairs = new Dictionary<string, Font>();

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    protected override void Initialization()
    {
        foreach( var item in array)
        {
            pairs.Add(item.name, item);
        }
    }

#if UNITY_EDITOR
    public Font EditorFont(string key)
    {
        if (array.Count != 0 && pairs.Count == 0)
        {
            Initialization();
        }
        Font result;
        pairs.TryGetValue(key, out result);
        return result;
    }
#endif
    public Font LoadFont(string key)
    {
        Font result;
        pairs.TryGetValue(key, out result);
        return result;
    }
}
