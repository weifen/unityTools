﻿public class Singleton<T>where T:new()
{
    static T value;
    static object _object = new object();
    public static T inst
    {
        get
        {
            lock (_object)
            {
                if(value == null)
                {
                    value = new T();
                    (value as Singleton<T>).Initialization();
                }
                return value;
            }
        }
    }

    protected virtual void Initialization()
    {

    }
}
