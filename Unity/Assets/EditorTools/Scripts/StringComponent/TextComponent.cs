﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TextComponent : Text
{
    public override string text
    {
        get
        {
            if(Application.isPlaying)
            {
                m_Text = LanguageManager.LoadString(m_Text);

            }
            return m_Text;
        }
        set
        {
            if (String.IsNullOrEmpty(value))
            {
                if (String.IsNullOrEmpty(m_Text))
                    return;
                m_Text = string.Empty;
                SetVerticesDirty();
            }
            else if (m_Text != value)
            {
                m_Text = value;
                SetVerticesDirty();
                SetLayoutDirty();
            }
        }
    }
}
