﻿using System.Collections.Generic;

using UnityEngine.UI;
using UnityEngine;

public class LanguageManager: MonoSingleton<LanguageManager>
{
    [SerializeField] Language language;
    public LanguageInfo[] array;
    Dictionary<string, LanguageInfo> pairs = new Dictionary<string, LanguageInfo>();

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    protected override void Initialization()
    {
        pairs.Clear();
        foreach(var item in array)
        {
            pairs.Add(item.key, item);
        }
    }

    public static string LoadString( string key,params string[] args)
    {
        if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(key.Trim()))
            return key;

        LanguageInfo result;
        if (inst.pairs.TryGetValue(key, out result))
        {
            switch (inst.language)
            {
                case Language.Chinese: key = result.chinese; break;
                case Language.English: key = result.english; break;
            }

            if(args.Length>0)
            {
                key = string.Format(key,args);
            }
        }

        return key;
    }
}

public enum Language
{
    Chinese,
    English
}
[System.Serializable]
public struct LanguageInfo
{
    public string key;
    public string chinese;
    public string english;
}