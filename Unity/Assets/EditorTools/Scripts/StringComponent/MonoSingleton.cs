﻿using UnityEngine;

public class MonoSingleton<T>:MonoBehaviour where T:MonoBehaviour
{
    static T _inst;
    static object _object = new object();
    public static T inst
    {
        get
        {
            lock (_object)
            {
                if(_inst == null)
                {
                    _inst = Object.FindObjectOfType<T>();
                    if(_inst==null)
                    {
                        var go = new GameObject();
                        _inst = go.AddComponent<T>();
                        go.name = typeof(T).Name;
                    }
                    GameObject.DontDestroyOnLoad(_inst.gameObject);
                    (_inst as MonoSingleton<T>).Initialization();
                }
                return _inst;
            }
        }
    }

    private void Reset()
    {
        _inst = null;
    }

    public static bool Exists()
    {
        return _inst != null;
    }

    protected virtual void Initialization()
    {

    }
}
