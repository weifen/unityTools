﻿using UnityEngine;
using System.Collections.Generic;

public static class TransformHelper
{
    public static Transform[] TransformChilds(this Transform transform)
    {
        List<Transform> result = new List<Transform>();
        Foreach(result,transform);
        return result.ToArray();
    }

    static void Foreach(List<Transform> result,Transform transform)
    {
        result.Add(transform);
        foreach( Transform child in transform)
        {
            Foreach(result, child);
        }
    }
}

