﻿using System;

namespace MyGUI
{
    public class TreeNodeHelper
    {
        public static void Ergodic<T>(TreeNode<T> node, Func<TreeNode<T>, bool> action)
        {
            if (action.Invoke(node))
            {
                foreach (TreeNode<T> child in node)
                {
                    Ergodic(child, action);
                }
            }
        }
    }
}