﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public static class FileHelper
{
    public static string [] GetFiles(params string[] suffix)
    {
        
        return Directory.GetFiles(Application.dataPath, "*.*")
            .Where(s => suffix.Contains(Path.GetExtension(s).ToLower())).ToArray();
    }

    public static string[] GetFiles(string rootPath, params string[] suffix)
    {
        return Directory.GetFiles(rootPath, "*.*")
            .Where(s => suffix.Contains(Path.GetExtension(s).ToLower())).ToArray();
    }

    public static string FindOneFile(string searchPattern,string path)
    {
        var result = Directory.GetFiles(Application.dataPath, searchPattern)
        .Where(s => s.IndexOf(path) != -1).FirstOrDefault();
        return result.Substring(result.IndexOf("Assets"));
    }
}