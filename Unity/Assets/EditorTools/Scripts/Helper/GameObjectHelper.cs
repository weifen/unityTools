﻿using UnityEngine;

public static class GameObjectHelper
{
    public static string GetPath(this GameObject go)
    {
        return GetTransformPath(go.transform);
    }
    public static string GetPath(this Transform tr)
    {
        return GetTransformPath(tr);
    }
    public static string GetTransformPath(Transform trans)
    {
        if (!trans.parent)
        {
            return trans.name;
        }
        return GetTransformPath(trans.parent) + "/" + trans.name;
    }



    public static string GetPath(this Transform tr,Transform root)
    {
        return GetTransformPath(tr,root);
    }
    public static string GetTransformPath(Transform trans,Transform root)
    {
        if (trans == root)
            return trans.name;
        if (!trans.parent)
        {
            return trans.name;
        }
        return GetTransformPath(trans.parent,root) + "/" + trans.name;
    }
}